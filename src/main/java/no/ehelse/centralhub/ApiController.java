package no.ehelse.centralhub;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import ca.uhn.fhir.model.dstu2.composite.QuantityDt;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.Device;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.dstu2.valueset.BundleTypeEnum;
import ca.uhn.fhir.model.dstu2.valueset.HTTPVerbEnum;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.client.IRestfulClientFactory;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v26.datatype.CX;
import ca.uhn.hl7v2.model.v26.group.ORU_R01_OBSERVATION;
import ca.uhn.hl7v2.model.v26.group.ORU_R01_ORDER_OBSERVATION;
import ca.uhn.hl7v2.model.v26.group.ORU_R01_PATIENT_RESULT;
import ca.uhn.hl7v2.model.v26.message.ORU_R01;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import ca.uhn.hl7v2.model.v26.segment.PID;
import ca.uhn.hl7v2.parser.EncodingNotSupportedException;
import ca.uhn.hl7v2.parser.Parser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.hl7.fhir.instance.model.api.IBaseResource;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.*;

// Done: EffectiveDateTime
// TODO: Tests
// TODO: Forward authentication token
// TODO: Device-observation
// TODO: Bundle

@RestController
public class ApiController {

    private static final Log logger = LogFactory.getLog(ApiController.class);

    @RequestMapping(value = "/pcd01", method = RequestMethod.POST, produces="application/txt")
    @ResponseBody
    public String processMessage(Principal principal, HttpServletResponse response, @RequestBody String pcdPacket) {

        response.setContentType("application/txt");

        String pcdPacketFormatted = pcdPacket.replace("#xD;","\r").replace("&amp;", "&");

        // Create FHIR client
        // We're connecting to a DSTU1 compliant server in this example
        FhirContext ctx = FhirContext.forDstu2();
        String serverBase = "http://fhir.ehelse.space/api";

        IGenericClient client = ctx.newRestfulGenericClient(serverBase);

        // Create a logging interceptor
        LoggingInterceptor loggingInterceptor = new LoggingInterceptor();

        // Optionally you may configure the interceptor (by default only
        // summary info is logged)
        loggingInterceptor.setLogRequestSummary(true);
        loggingInterceptor.setLogRequestHeaders(true);
        loggingInterceptor.setLogRequestBody(true);
        //loggingInterceptor.setLogResponseBody(true);

        // Register the interceptor with your client (either style)
        client.registerInterceptor(loggingInterceptor);

		/*
           * The HapiContext holds all configuration and provides factory methods for obtaining
           * all sorts of HAPI objects, e.g. parsers.
           */
        HapiContext context = new DefaultHapiContext();
        Parser p = context.getGenericParser();

        Message hapiMsg;
        try {
            // The parse method performs the actual parsing
            hapiMsg = p.parse(pcdPacketFormatted);
        } catch (EncodingNotSupportedException e) {
            e.printStackTrace();
            return e.toString();
        } catch (HL7Exception e) {
            e.printStackTrace();
            return e.toString();
        }

        ORU_R01 oruMsg = (ORU_R01) hapiMsg;
        MSH msh = oruMsg.getMSH();

        try {
            logger.info("Trying to find patient"+oruMsg.printStructure());
        } catch (HL7Exception e) {
            e.printStackTrace();
        }

        // Patient ID, name, address etc.
        ORU_R01_PATIENT_RESULT patientResult = oruMsg.getPATIENT_RESULT();
        //String patientId = patientResult.getPATIENT().getPID().getPatientID().getIDNumber().toString();
        PID pid = patientResult.getPATIENT().getPID();
        CX[] cx = pid.getPid3_PatientIdentifierList();
        String patientId = "unknown";

        try  {
            logger.info("Handling message for patient="+cx[0].toString());
            patientId = cx[0].toString().
                    replace("CX", "").
                    replace("NI", "").
                    replace("[", "").
                    replace("]","").
                    replace("^","").
                    replace("&amp;","").
                    replace("&",""); //cx[0].toString().replace("CX[","").replace("]","").trim();
            logger.info("PatientId:"+patientId);
        } catch (Exception e) {
            logger.error("Exception while getting patient id: "+e.toString());
            return "Error while getting patient id "+e;
        }

        logger.info("Handling message for patientid="+patientId);

        Observation observation = new Observation();

        ResourceReferenceDt patientReference = new ResourceReferenceDt();
        patientReference.setReference("Patient/"+patientId);
        observation.setSubject(patientReference);

        logger.info("Observation:" + observation.toString());

        // Create device resource
        Device device = new Device();
        device.setPatient(patientReference);

        // Enter measurement data
        processMeasurementInformation(patientResult.getORDER_OBSERVATION(), observation, device);

        // Create bundle
        Bundle bundle = new Bundle();
        bundle.setType(BundleTypeEnum.TRANSACTION);

        Bundle.Entry obsEntry = new Bundle.Entry();
        Bundle.EntryRequest obsRequest = new Bundle.EntryRequest();
        obsEntry.setResource(observation);
        obsRequest.setMethod(HTTPVerbEnum.POST);
        obsRequest.setUrl("Patient");
        obsEntry.setRequest(obsRequest);
        bundle.addEntry(obsEntry);

        Bundle.Entry devEntry = new Bundle.Entry();
        Bundle.EntryRequest devRequest = new Bundle.EntryRequest();
        devEntry.setResource(device);
        devRequest.setMethod(HTTPVerbEnum.PUT);
        devRequest.setUrl("Device/"+device.getId());
        devEntry.setRequest(devRequest);
        bundle.addEntry(devEntry);

        // Submit FHIR resource
        /*
        MethodOutcome outcome = client.create()
                .resource(observation)
                .prettyPrint()
                .encodedJson()
                .execute();
                */

        Bundle bundleResponses = client.transaction().withBundle(bundle).execute();

        String result = "Unknown result";

        try {
            result = bundleResponses.getId().toString();
            result += ","+ bundleResponses.getEntry().get(0).getResponse().getStatus()+":"+
                    bundleResponses.getEntry().get(0).getResponse().getLocation();
            result += ","+ bundleResponses.getEntry().get(1).getResponse().getStatus()+":"+
                    bundleResponses.getEntry().get(1).getResponse().getLocation();
            //IdDt id = (IdDt) outcome.getId();
            //result = id.getValue();
        } catch (Exception e) {
            logger.info("Exception:"+e.toString(),e);
            return "Exception:"+e.toString()+" - "+result;
        }

        return result;

    }

    private String processMeasurementInformation(ORU_R01_ORDER_OBSERVATION order_observation,
                                                 Observation observation, Device device) {

        CodeableConceptDt codeableConcept = new CodeableConceptDt();
        List<CodingDt> codingList = new ArrayList<CodingDt>();
        List<Observation.Component> componentList = new ArrayList<Observation.Component>();

        String retstr = "";

        try {

            List<ORU_R01_OBSERVATION> obs = order_observation.getOBSERVATIONAll();
            logger.info("obs number="+obs.size());

            for(ORU_R01_OBSERVATION ob : obs) {

                String sensor = ob.getOBX().getObx3_ObservationIdentifier().getComponent(1).toString();

                if (sensor.indexOf("MDC_MOC_VMS_MDS_AHD") != -1) {
                    //  OBX|1||531981^MDC_MOC_VMS_MDS_AHD^MDC|0|||||||X|||||||ECDE3D4E58532D31^^ECDE3D4E58532D31^EUI-64
                    device.setId(ob.getOBX().getObx18_EquipmentInstanceIdentifier().clone()[0].getUniversalID().toString());
                    logger.info("Device id:"+device.getId());
                }
                else if (sensor.indexOf("MDC_ID_MODEL_MANUFACTURER") != -1) {
                    //OBX - OBX|12|ST|531970^MDC_ID_MODEL_MANUFACTURER^MDC|1.0.0.1|Lamprey Networks||||||R
                    //"manufacturer":"A&D Medical ",
                    //        "model":"UA-651BLE ",
                    device.setManufacturer(ob.getOBX().getObx5_ObservationValue().clone()[0].toString());
                    logger.info("Device manufacturer:"+device.getManufacturer());
                }
                else if (sensor.indexOf("MDC_ID_MODEL_NUMBER") != -1) {
                    //OBX|13|ST|531969^MDC_ID_MODEL_NUMBER^MDC|1.0.0.2|Blood Pressure 1.0.0||||||R
                    device.setModel(ob.getOBX().getObx5_ObservationValue().clone()[0].toString());
                    logger.info("Model:"+device.getModel());
                }
                else if (sensor.indexOf("MDC_ATTR_TIME_ABS") != -1) {
                    // OBX|21|DTM|67975^MDC_ATTR_TIME_ABS^MDC|1.0.0.7|20130301115423.00||||||R|||20130301115450.733-0500
                    // "effectiveDateTime": "2015-10-20T07:44:44.361+02:00",

                    logger.info("Day:"+ob.getOBX().getDateTimeOfTheObservation().getValue()+ " - "+
                                    ob.getOBX().getDateTimeOfTheObservation().getHour()+":"+
                                    ob.getOBX().getDateTimeOfTheObservation().getMinute()+", "+
                                    ob.getOBX().getDateTimeOfTheObservation().getDay()+"/"+
                                    ob.getOBX().getDateTimeOfTheObservation().getMonth()+" "+
                                    ob.getOBX().getDateTimeOfTheObservation().getYear()
                    );

                    // Setting effective date time
                    try {
                        DateTimeDt dateTime = new DateTimeDt();
                        dateTime.setValue(ob.getOBX().getDateTimeOfTheObservation().getValueAsDate());
                        observation.setEffective(dateTime);
                    }
                    catch (Exception e) {
                        logger.error("Date parsing exception:"+e);
                    }


                } else if (sensor.indexOf("CERT_DATA") == -1 &&
                        sensor.indexOf("MOC_VMS") == -1 &&
                        sensor.indexOf("TIME_SYNC") == -1 &&
                        sensor.indexOf("DEV_SPEC") == -1 &&
                        sensor.indexOf("MDC_ID_") == -1 &&
                        sensor.indexOf("MDC_ATTR") == -1 &&
                        sensor.indexOf("MDC_REG_CERT_") == -1 &&
                        sensor.indexOf("MDC_ATTR_REG") == -1 &&
                        sensor.indexOf("MDC_TIME") == -1 &&
                        sensor.indexOf("MDC_ATTR_TIME") == -1) {

                    String value = ob.getOBX().getObservationValue(0).getData().encode();
                    String code = ob.getOBX().getObx3_ObservationIdentifier().getComponent(0).toString();

                    if (ob.getOBX().getObx2_ValueType() == null || ob.getOBX().getObx2_ValueType().isEmpty()) { // No value. Assume (naively) it's a code

                        logger.info("Interpreting as code for whole Observation");
                        logger.info("Sensor:" + sensor+ ", Value:" + value + ", Code:" + code);

                        CodingDt coding = new CodingDt();
                        coding.setCode(code);
                        //coding.setSystem("urn:std:iso:11073:10101");
                        coding.setDisplay(sensor);
                        codingList.add(coding);

                    } else {

                        logger.info("Interpreting as component value and code");
                        logger.info("Sensor:" + sensor+ ", Value:" + value + ", Code:" + code);

                        String componentCode = ob.getOBX().getObx6_Units().getComponent(0).toString();
                        String componentCodeDisplay = ob.getOBX().getObx6_Units().getComponent(1).toString();

                        Observation.Component component = new Observation.Component();
                        CodeableConceptDt componentCodeableConcept = new CodeableConceptDt();
                        List<CodingDt> componentCodingList = new ArrayList<CodingDt>();

                        // Coding
                        CodingDt coding = new CodingDt();
                        coding.setCode(code);
                        //coding.setSystem("urn:std:iso:11073:10101"); // Should this be hard-coded?
                        coding.setDisplay(sensor);
                        componentCodingList.add(coding);

                        // Value
                        QuantityDt quantityValue = new QuantityDt();
                        try {
                            quantityValue.setValue(Float.parseFloat(value));
                        } catch (Exception e) {
                            logger.info("Exception while parsing value (" + value + ") " + e);
                        }

                        //quantityValue.setSystem("urn:std:iso:11073:10101"); // Should this be hard-coded?
                        quantityValue.setCode(componentCode);
                        quantityValue.setUnit(componentCodeDisplay); // Not always very readable unit...

                        componentCodeableConcept.setCoding(componentCodingList);
                        component.setCode(componentCodeableConcept);
                        component.setValue(quantityValue);
                        componentList.add(component);
                    }
                } else {
                    logger.info("Not adding sensor:" + sensor);
                }

            }

            // Populate the observation with the list of codes and values
            codeableConcept.setCoding(codingList);
            observation.setCode(codeableConcept);
            observation.setComponent(componentList);

        }
        catch (HL7Exception e) {
            System.out.println(e.toString());
        }

        return retstr;
    }

}
